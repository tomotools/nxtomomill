Change Log
==========

1.2.0: unreleased
-----------------

* h52nx
    * avoid creating master file by default. Replace '--no-master-file' option by '--with-master-file'


1.1.0: 2024/12/06
-----------------

* h52nx
    * add '--no-master-file' option (PR 272)
    * fix cropping of machine current points (PR 177)
    * fix motor geometry (PR 195)
    * add source to sample distance (PR 202)
    * improve machine electric current deduction (PR 179)
    * add ROI (PR 172)
* edf2nx
    * add an option 'using the real angle values' (PR 181)

* misc
    * move progress to tqdm (PR 187)
    * remove '3D-XRD', 'XRD-CT' classes (PR 196)
    * remove 'require_x_translation', 'require_z_translation' (PR 199)
    * remove usage of 'scan_numbers' (PR 173)
    * move to '__future__' annotations (PR 166)
    * move sources to 'src' (PR 130)

1.0.11: 2024/10/07
------------------

* h52nx: modify bliss file scan ordering (based on each scan 'start_time' when existing)

1.0.9:  2024/08/27
------------------

* h52nx: handle z-series v 3 (PR 258)

1.0.0:  2024/02/23
------------------

* app
    * add `nx-copy`: application to copy NXtomo contained in a file (Update relative links of the HDF5 VDS if any). (PR 188)

* misc
    * remove 'is_xrdct_entry' (PR 209)
    * rework doc and move to sphinx-pydata-theme
    * move to silx 2.0

0.13.2: 2023/08/03
------------------

* converter
    * hdf5:
        * add bliss_orginal_files option

0.13.0: 2023/08/01
------------------

* converter
    * remove x and y real pixel size metadata
    * remove magnification metadata
    * edf2
        * add option `output-checks`: allow check on the generated volume (PR 155)
        * add option `delete-edf`: allow deletion of the edf input files once the conversion is done (and check if asked) (PR 155)
    * hdf5
        * benefit from 'technique/image' bliss metadata when possible (PR 147, 168)
        * remove plugins system as never used (PR 177)
        * improve pcotomo robustness (PR 160, PR 159)
        * `estimated_cor_from_motor` is now set for 360 degree scan (and not only 180) (PR 161)
        * improve robustness against cancel scan (PR 158, 165)
        * remove `real pixel size` and `magnification`
* nexus
    * improve robustness and readibility of node name vs path handling (PR 173)
* app
    * add `edf2nx-check`: application to allow check from a previous conversion and optionnaly remove EDF source files. (PR 164)
    * add `split-nxfile`: application to split a file containing several NXtomo into single-NXtomo files. (PR 172)
    * add `z-concatenate-scans`: application to concatenate a z-serie of scan into a single NXtomo (PR 174, 175).
    * `zstages2nxs`: add `output_filename_template` option (PR 176)
    * deprecate `h5-quick-start` in favor of `h5-config` (PR 166)
    * deprecate `edf-quick-start` in favor of `edf-config` (PR 166)

* misc
    * deprecate `from_dx_to_nx` for `from_dx_config_to_nx` (PR 167)
    * replace string.format by f-strings when possible (PR 154)
    * doc: add an example of btaach processing

0.12.0: 2023/02/23
------------------

* app
    * `zstages2nxs` command added (PR 145)
* converter
    * hdf5
        * add frame flip information (PR 109)
    * edf2nx
        * better handling of current units (PR 150)
* misc
    * get rid of numpy distutils for packaging (PR 151)


0.11.0: 2022/12/15
------------------

* converter
    * hdf5:
        * handle new path with {detector_name}
        * handle flips
        * fix: was unable to find pixel position and energy for zserie 

0.10.9: 2022/10/26
------------------

* converter
    * hdf5converter: handle EBStomo pcotomo second version


0.10.1: 2022/08/31
------------------

* converter
    * EDFConfig: default unit for distance, x, y and z translations from .info file is now millimeter
    * edf2nx: fix deduction of field of view.


0.10.0: 2022/08/30
------------------

* converter
    * remove deprecated 'h5_to_nx' function (deprecated since 0.5.0)
    * hdf5converter
        * add management of magnification (PR 110). Not handled for by edfconverter
        * add function to do a subselection of an NXtomo from the rotation angle and add dedicated parameter for pcotomo conversion to refine NXtomo to be created (PR 100)

    * edf2nx
        * add an option to avoid data duplication (PR 114, 115, 118)

* nexus
    * add probe to NXsource (PR 117)
    * improve NXdetector.data setter (PR 122)
    * rename attributes `unit` to `units`


0.9.0: 2022/06/24
-----------------

* converter
    * hdf5
        * add handling of the machine electrical current (PR 106)
        * add management of `is_rearranged` attribute (PR 111)
    * edf
        * add management of a configuration file (PR 104)

* nexus
    * add Nxtomo concatenation (PR109)

0.8.0: 2021/06/04
-----------------

* converter
    * hdf5
        * add 'bam_single_file' option (PR 96)
        * add pcotomo management (PR 91, 88)

* add nexus module providing API to edit an NXtomo (PR 87)


0.7.0: 2021/01/07
-----------------

* converter
    * hdf5: management of ExternalLink as entry (case of bliss proposal file, PR 85)
    * edf
        * fix issues with progress (PR 80)

* patch-nx
    * add option to convert all frame of a given type (PR 84)

* dxfile2nx
    * allow to provide pixel size as a single value (PR 77)

* miscellaneous::
    * add some missing aliases flat/ref (PR 83)
    * benefit from validator (PR 81)


0.6.0: 2021/10/04
-----------------

* app
    * add 'dxfile2nx' application. Allow to convert from dxfile format to NXtomo format
    * h52nx
        * add "duplicate_data" option to force frame duplication from the command line.
    * add 'h5-3dxrd-2nx': convert from to 3D XRD - bliss-hdf5 to (enhance) NXtomo Format .

* converter
    * add dxfileconverter


0.5.0: 2021/04/20
-----------------

* converter
    * hdf5
        * rework virtual dataset creation
        * insure the number of projection found is that same as the expected number of projection (tomo_n)
        * add `start_time` and `end_time`
        * handle configuration file
            * add `HDF5config` and `HDF5ConfigHandler`
        * improve research of keys / path. First try to retrieve a dataset with the expected number of elements. If fail return the first dataset fitting a path.
        * add `ignore_sub_entries` to skip some scan
        * add a warning if no acquisition are found
    * utils
        * rework `_insert_frame_data` to use a class instead `_FrameAppender`
        * fix management of negative indexing
* app
    * patch-nx
        * add an option `--embed-data`
    * rename `tomoh52nx` to `h52nx`. Deprecate `tomoh52nx`.
    * rename `tomoedf2nx` to `edf2nx`. Deprecate `tomoedf2nx`.
    * `h52nx`
        * can now take a configuration file in parameter ("--config" option)
        * add option `--ignore-sub-entries`


0.4.0: 2020/11/09
-----------------

* requires h5py >= 3
* utils:
    * add `change_image_key_control` function to modify frame type inplace
    * add `add_dark_flat_nx_file` function to add dark or flat in an existing `NXTomo` entry
    * converter
        * h5_to_nx:
            * add management of 'proposal file': handle External / SoftLink
            * insure relative path is given when converting the file
            * magnified_pixel_size will not be write anymore. If a magnified / sample pixel size is discover then this will be saved as the 'pixel_size'.
            * add an option to display_advancement or not.
            * split "zseries" according to z value
            * add NXdata information to display detector/data from root as image
        * move format version to 1.0

* app:
    * add patch-nx application to modify an existing `NXTomo`
        * add dark or flat series
        * modify frame type
    * tomoh52nx:
        * warning if we try to convert a file containing some `NXTomo` entry
        * create directories for output file if necessary
        * check write rights on output file
        * split "zseries" according to z value

* miscellaneous::
    * adopt 'black' coding style


0.3.4: 2020/10/05
-----------------

* converter: fix log

0.3.3: 2020/08/26
-----------------

* h5_to_nx:
    * add set-param option to let the user define some parameters values like energy if he knows it is missing (and avoid asking him n times).
* io: add management of hdf5 files from tomoscan.io.HDF5File

0.3.1: 2020/08/19
-----------------

* add field_of_view parameter

* add plugin management (allows user to define motor position value from a python script) - PR !19


0.3.0: 2020/03/20
-----------------

* app: add several option to define titles

* h5_to_nx:
    * add possibility to add plugins for defining new motor position
    * units: move distances to meter

* edf_to_nx:
    * units: move distances to meter


0.2.0: 2020/22/04
-----------------

* setup: add entry point on __main__

* converter
    * `h5_to_nx`: add a possible callback to give input

* doc
    * add API documentation
    * add tutorials for `tomoedf2nx` and `tomoh5tonx`


0.1.0: 2020/03/12
-----------------

* app
    * add application `tomoedf2nx`: convert acquisition using old bliss and EDF to .hdf5 file format, nexus (NXtomo) compliant format.
    * add application `tomoh5tonx`: convert acquisition using bliss/hdf5 to a nexus (NXtomo) compliant format.

* converter
    * add `h5_to_nx` function to convert from bliss .hdf5 to nexus (NXtomo) compliant format.
    * add `get_bliss_tomo_entries` function to return the bliss 'roor' entries (for now named 'tomo:basic', 'tomo:fullturn' ...)
