.. _nxtomo_struct_frm_h52nx:

Structure of the NXtomo generated by h52nx
''''''''''''''''''''''''''''''''''''''''''

The h52nx application allow users to generated NXtomo(s) from bliss file (see :ref:`Tomoh52nx`).

The resulting `NXtomo application <https://manual.nexusformat.org/classes/applications/NXtomo.html#nxtomo>`_ will be an entry in a HDF5 file.

Almost all datasets of the NXtomo is a copy of the bliss raw data.
This allows users to modify those information without affecting the raw data.

**Raw data is considered as 'the truth' and should never be modified** by the user or a data analysis software.

Nethertheless we cannot copy all the frame (which represent 99.9% of the data size).
Today the `h52nx` application will create a `virtual dataset <https://docs.h5py.org/en/stable/vds.html>`_ to provide access to the frames (instrument/detector/data field).

If you want to edit / modify those frame it is recommended to use `nxtomo project <https://gitlab.esrf.fr/tomotools/nxtomo>`_ which will help you modify the NXtomo and will prevent as much as possible to 
edit the raw frames.

Please when you access the raw data frame directly with h5py make sure you open then in 'read only' mode to be safer.
