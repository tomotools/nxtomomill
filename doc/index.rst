.. nxtomomill documentation

.. |foo| image:: img/nxtomomill.png
    :scale: 22 %


============================================
|foo| Welcome to nxtomomill's documentation!
============================================

nxtomomill provide a set of applications to convert tomography acquisition made by `BLISS <https://www.esrf.fr/BLISS>`_ from their original file format (.edf, .h5) to a Nexus compliant file format (using `NXtomo <https://manual.nexusformat.org/classes/applications/NXtomo.html>`_)

.. deprecated:: 1.0

    The `nexus` module allowing users to easily edit an NXtomo has been moved to `nxtomo project <https://gitlab.esrf.fr/tomotools/nxtomo>`_

.. toctree::
   :maxdepth: 1
   :hidden:

   tutorials/index.rst
   userguide/index.rst
   api.rst
   development/index.rst
