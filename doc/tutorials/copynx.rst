.. _copynxtutorial:

copy-nx tutorial
================

.. article-info::
    :read-time: 5 min read
    :class-container: sd-p-2 sd-outline-muted sd-rounded-1

the `copy-nx` command can be used to copy one or several NXtomo contained in a file to another location.

Examples
--------

copy a single file (with potentially several NXtomo)
````````````````````````````````````````````````````

.. code-block:: bash

    nxtomomill nx-copy /path/to/nxomofile.nx .
    nxtomomill nx-copy /path/to/nxomofile.nx new.nx


copy a single NXtomo (potentially contained in a file with several NXtomo)
``````````````````````````````````````````````````````````````````````````

.. code-block:: bash

    nxtomomill nx-copy /path/to/nxomofile.nx --entry entry0000  .
    nxtomomill nx-copy /path/to/nxomofile.nx --entries entry0000,entry0001  .


copy a set of file
``````````````````

.. code-block:: bash

    for file in *.nx
    do
        nxtomomill nx-copy "$file" /new/path/"$file"
    done


Virtual dataset behavior
------------------------

If the source file contains HDF5 virtual dataset then the virtual source will be updated to fix relative links.

You can also use the 'remove-vds' option. In this case the HDF5 - virtual dataset will be replaced by a standard HDF5 dataset (getting rid of the virtual link). But be aware that with this option full dataaset must be able to fill in memory in order to modify it (at least for now).
