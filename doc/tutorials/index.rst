Tutorials
=========

.. toctree::
   :hidden:

   edf2nx.rst
   h52nx.rst
   patch_nx.rst
   dxfile2nx.rst
   copynx.rst


.. grid:: 3

   .. grid-item-card::

      :doc:`edf2nx`

   .. grid-item-card::

      :doc:`h52nx`

   .. grid-item-card::

      :doc:`patch_nx`

   .. grid-item-card::

      :doc:`dxfile2nx`

   .. grid-item-card::

      :doc:`copynx`

.. hint::

   You can also find a NXtomo tutorials on the `nxtomo project <https://tomotools.gitlab-pages.esrf.fr/nxtomo/>`_ here: https://tomotools.gitlab-pages.esrf.fr/nxtomo/tutorials/index.html
