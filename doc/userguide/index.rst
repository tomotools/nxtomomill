User Guide
==========

.. toctree::
   :maxdepth: 1

   installation.rst
   user_corner.rst
   settings.rst