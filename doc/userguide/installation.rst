Installation
''''''''''''

The simplest way to install nxtomomill is to use the pypi wheel provided ()

.. code-block:: bash

   pip install nxtomomill


You can also install it from `source <https://gitlab.esrf.fr/tomotools/nxtomomill>`_:


.. code-block:: bash

   pip install git+https://gitlab.esrf.fr/tomotools/nxtomomill.git


Building doc
''''''''''''

In order to build documentation make sure you have installed the 'doc' extra dependancies

.. code-block:: bash

    pip install .[doc]

Then you can build the documentation using sphinx:

.. code-block:: bash

    sphinx-build doc html
