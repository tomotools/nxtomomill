Settings
========

nxtomomill needs to get several information in order to complete translation from EDF or (bliss) HDF5 to nexus-compliant HDF5 file.

This information
   * is a set of key values for EDF
   * is a set of dataset location and name for (bliss) HDF5

All are stored in nxtomomill.settings.py file. For `tomoh52nx` you can generally overwrite from calling the appropriate command option  (see :ref:`Tomoh52nx`).

But for scripting are when used from a dependency (like tomwer) it can be convenient to modify some of them.
For example if you cannot retrieve rotation angle because this information is stored at a different location than the 'default' one knows by nxtomomill you can change `ROT_ANGLE_KEYS` parameter.
