User corner
===========

Batch processing
****************

It can be convenient to convert several file in a row. Here are two small examples around h52nx that can be reused for other nxtomomill applications.

using python script
^^^^^^^^^^^^^^^^^^^

In this example we show how to retrieve a list of file to be converted and them convert them one by one

.. code-block:: python

    from glob import glob
    from nxtomomill.converter import from_h5_to_nx
    from nxtomomill.io.config import TomoHDF5Config
    from tqdm import tqdm


    bliss_files=glob("/home/payno/Datasets/tomography/tomwer/tomwerExtraDatasets/bamboo_hercules//*.h5")
    for file_path in bliss_files:
        print(f"start conversion of {file_path}")
        config = TomoHDF5Config()
        config.input_file = file_path
        config.output_file = file_path.replace(".h5", ".nx")
        from_h5_to_nx(
            configuration=config,  # you can tune the conversion using this class
            progress=tqdm(desc="h52nx"),  # used to display advancement
        )


using bash
^^^^^^^^^^

In this example we show how to retrieve a list of file to be converted and them convert them one by one

.. code-block:: bash

    # source tomotools environment
    source /scisoft/tomotools/activate dev

    # list all bliss file to be converted
    bliss_files=$(ls -d /data/visitor/{xxx}/RAW_DATA/*/*/*.h5)

    for file_path in $bliss_files
    do
        echo "start conversion of $file_path"
        nxtomomill h52nx $file_path   # + possible options like config file or inline options
    done
