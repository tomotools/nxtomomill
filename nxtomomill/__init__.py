"""a set of applications and tools around the `NXtomo <https://manual.nexusformat.org/classes/applications/NXtomo.html>`_ format defined by the `NeXus community <https://manual.nexusformat.org/index.html>`_"""

from .version import version as __version

__version__ = __version
